package deploylego

import (
	"gitlab.com/project_falcon/kubeless/klib/job"
	"gitlab.com/project_falcon/kubeless/klib/podlego"
	"gitlab.com/project_falcon/kubeless/klib/toolslego"
	"gitlab.com/project_falcon/kubeless/lib/payload"
	appsV1 "k8s.io/api/apps/v1"
	apiV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func ConvertPodSpecToPayload(podSpec apiV1.PodSpec) payload.Pods {
	pod := payload.Pods{}

	return pod
}

func getSelector(apiEvent *payload.APIData) *metaV1.LabelSelector {
	labels := make(map[string]string)
	labels["deploy"] = toolslego.GetBaseName(apiEvent.Action, apiEvent.Cluster.Box.Project)

	selector := metaV1.LabelSelector{
		MatchLabels: labels,
	}

	return &selector
}

func GetDeployTemplate(apiEvent *payload.APIData, namespace string, jobDef *job.YamlRoot, kconfig *toolslego.KConfig) (*appsV1.Deployment, error) {

	// jobDefinitions.
	initContainers, err := podlego.GetContainers(apiEvent, jobDef.Fc2.Kbox.Init, kconfig, apiEvent.Cluster.Box, jobDef.Fc2.Kbox.Default)
	if err != nil {
		return nil, err
	}

	container, err := podlego.GetContainers(apiEvent, jobDef.Fc2.Kbox.Run, kconfig, apiEvent.Cluster.Box, jobDef.Fc2.Kbox.Default)
	if err != nil {
		return nil, err
	}

	deployName := toolslego.GetBaseName(apiEvent.Action, apiEvent.Cluster.Box.Project)
	deployLabels := toolslego.GetBoxLabels(*apiEvent.Cluster.Box, deployName)

	deployMap := make(map[string]string)
	deployMap["type"] = "deploy"


	podLables := toolslego.MergeMaps(deployLabels, getSelector(apiEvent).MatchLabels)
	podLables = toolslego.MergeMaps(podLables, deployMap)

	deployment := appsV1.Deployment{

		ObjectMeta: metaV1.ObjectMeta{
			Name:      deployName,
			Namespace: namespace,
			Labels:    deployLabels,
		},

		Spec: appsV1.DeploymentSpec{
			Replicas: toolslego.Int32Ptr(1),
			Selector: getSelector(apiEvent),
			Template: apiV1.PodTemplateSpec{
				ObjectMeta: metaV1.ObjectMeta{
					Name:   deployName,
					Labels: podLables,
				},
				Spec: apiV1.PodSpec{
					RestartPolicy: apiV1.RestartPolicyAlways,
					// NodeSelector: ,
					InitContainers: initContainers,
					Containers:     container,
					Volumes:        podlego.GetVolumens(jobDef.Fc2.Kbox),
				},
			},
		},
	}

	return &deployment, err
}
